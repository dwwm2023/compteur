<?php

// Démarrez la session
session_start();

include "connect_bdd.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $error = '';

    //check tous les champs non vide
    if (empty($name) or empty($firstname) or empty($number) or empty($street) or empty($zipcode) or empty($city)) {
        $error = 'les champs ne doivent pas être vide';
    } else { 
        // Sécurisez les données POST en utilisant des requêtes préparées
        $idClient = htmlspecialchars($_POST["idClient"]);
        $name = htmlspecialchars($_POST["name"]);
        $firstname = htmlspecialchars($_POST["firstname"]);
        $number = htmlspecialchars($_POST["number"]);
        $street = htmlspecialchars($_POST["street"]);
        $zipcode = htmlspecialchars($_POST["zipcode"]);
        $city = htmlspecialchars($_POST["city"]);
        $insee = htmlspecialchars($_POST["insee"]);
    }

    // Créez l'enregistrement si aucune erreur n'est survenue
    if (empty($error)) {
        // Préparez la requête
        $query = $bdd->prepare('INSERT INTO studeffist (id, name, firstname, number, street, zipcode, city, insee)
        VALUES (:id, :name, :firstname, :number, :street, :zipcode, :city, :insee)');

        // Exécutez la requête avec les données du formulaire
        $query->execute(
            array(
                'id' => $idClient,
                'name' => $name,
                'firstname' => $firstname,
                'number' => $number,
                'street' => $street,
                'zipcode' => $zipcode,
                'city' => $city,
                'insee' => $insee
            )
        );

        // Redirigez l'utilisateur après l'insertion
        header('Location: index.php');
        exit();
    }
}

include "includes/header.php";

if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token'])) {
    exit("token périmé");
}

?>

<body>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-6">
                <div class="form">
                    <div class="head_form">
                        <h1 class="text-center">Ajout Compteur</h1>
                    </div>

                    <form class="row g-3 needs-validation" method="POST" novalidate>
                    <div class="row g-3 needs-validation" novalidate>
                    <div class="chkfrm">
                        <span><?php if (isset($error)) {echo $error;} ?></span>
                    </div>
                <div class="firstName col-md-4">
                    <label for="validationCustom01" class="form-label">numéro client :</label>
                    <input type="text" class="form-control" name="idClient" id="validationCustom01" r>
                </div>

                <div class="firstName col-md-7">
                </div>
                

                <div class="firstName col-md-5">
                    <label for="validationCustom02" class="form-label">Nom :</label>
                    <input type="text" class="form-control" name="name" id="validationCustom02" r>
                </div>

                <div class="lastName col-md-6">
                    <label for="validationCustom03" class="form-label">Prénom :</label>
                    <input type="text" class="form-control" name="firstname" id="validationCustom03"  >
                </div>

                <div class="number col-md-2">
                    <label for="validationCustom04" class="form-label">N° rue :</label>
                    <input type="number" class="form-control" name="number" id="validationCustom04" >
                </div>
    
                <div class="road col-md-6">
                    <label for="validationCustom05" class="form-label">Nom de rue :</label>
                    <input type="text" class="form-control" name="street" id="validationCustom05" >
                </div>
    
                <div class="mail col-md-3">
                </div>

                <div class="zip col-md-3">
                    <label for="validationCustom06" class="form-label">Code postal :</label>
                    <input type="text" class="form-control" name="zipcode" id="validationCustom06" >
                </div>

                <div class="city col-md-4">
                    <label for="validationCustom07" class="form-label">Ville :</label>
                    <select class="form-select" name="city" id="validationCustom07" >Ville...</option>
                    </select>
                </div>

                <div class="city col-md-3">
                    <label for="validationCustom08" class="form-label">INSEE :</label>
                    <input type="text" class="form-control" name="insee" id="validationCustom08" >
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" type="submit" onclick="validate()">Valider</button>
                </div>
            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>















<script src="script.js"></script>
</body>
</html>
