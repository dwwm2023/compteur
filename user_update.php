<?php
include "connect_bdd.php";

if (!empty($_GET['id'])) {
    // Vérifiez si le formulaire a été soumis
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $error = '';

        // check login
        $id_login = $_GET['id'];
        $chk_name = $bdd->prepare('SELECT * FROM studeffiusers WHERE id=:id');
        $chk_name->execute([
            'id' => $_GET['id']
        ]);
        $result = $chk_name->fetch();

        if ($_POST['login'] !== $result['login']) {
           // is email exist ?
           $chk_login = $bdd->prepare('SELECT * FROM user WHERE login = :login');
           $chk_login->execute([':login' => $_POST['login']]);
           $count = $chk_login->fetchColumn();

           if ($count > 0) {
               $error = 'nom d\'utilisateur déjà utilisé.';
           } 
        } 

      
        if (!$error) {
            // Update admin
            $admin = isset($_POST['admin']) ? 1 : 0;
            // UPDATE
            $query = $bdd->prepare('UPDATE studeffiusers SET admin=:admin WHERE id=:id');
            $query->execute([
                'admin' => $admin,
                'id' => htmlspecialchars($_GET['id'])
            ]);

            // Redirect after update
            header('Location: admin_users.php?id=' . $_GET['id'] . '&modify=ok');
            exit;
        }
        
    }

    include "includes/header.php";

    if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token'])) {
        exit("token périmé");
    }

    // Sélectionnez les données de l'utilisateur à mettre à jour
    $query = $bdd->prepare('SELECT * FROM studeffiusers WHERE id=:id');
    $query->execute([
        'id' => $_GET['id']
    ]);

    $data = $query->fetch();
} else {
    header('Location: admin_users.php');
    exit;
}
?>

<section>
    <div class="contentBox">
        <div class="formBox">
            <h2>Update user by admin</h2>
            <form action="#" method="POST">
                <div class="chkfrm">
                        <span><?php if (isset($error)) {echo $error;} ?></span>
                    </div>
                <div class="inputBx write">
                <span>Nickname</span>
                    <input type="text" name="login" id="login" value="<?php echo $data['login'] ?>">
                </div>
                <div class="inputBx write check">
                    <span>rendre administrateur ?</span>
                    <input type="checkbox" name="admin" <?php echo $data['admin'] ? 'checked' : ''; ?>>
                </div>
                <div class="inputBx">
                    <input type="submit" value="Register">
                </div>
            </form>
        </div>
    </div>
</section>
