function showResponsiveMenu() {
    let menu = document.getElementById("topnav_responsive_menu");
    let icon = document.getElementById("topnav_hamburger_icon");
    let root = document.getElementById("root");
    if (menu.className === "") {
      menu.className = "open";
      icon.className = "open";
      root.style.overflowY = "hidden";
    } else {
      menu.className = "";                    
      icon.className = "";
      root.style.overflowY = "";
    }
  }

  document.addEventListener('DOMContentLoaded', function() {
    // Obtenir l'URL de la page
    const currentPage = window.location.pathname;

    // Comparer l'URL avec les URLs de vos pages
    if ((currentPage.includes("index_php.php"))) {
            let links = document.querySelectorAll("#nav li");
            icons.addEventListener("click", () => {
            nav.classList.toggle("active");
        });
        
        links.forEach((link) => {
          link.addEventListener("click", () => {
            nav.classList.remove("active");
          });
        });
    }
});

/*--------------Filtre des villes en fonction du code postal------------*/
//création de variable pour le champ du code postal
let checkzip = document.getElementById('validationCustom06');
//contrôle si le champ pour le code postal est pas nul
if (checkzip != null) {
    //création d'évenement d'écoute et appel de la fonction addCity
    checkzip.addEventListener('blur', addCity);
    // let codePostal = this.value;
}

/*--------------         Recherche du code INSEE            ------------*/
//création de variable pour le champ ville
let checkcity = document.getElementById('validationCustom07');
//contrôle si le champ pour le champ ville est pas nul
if (checkcity != null) {
    //création d'évenement d'écoute et appel de la fonction searchINSEE
    checkcity.addEventListener('blur', getCodeINSEE);
}

const codePostal = document.querySelector('#validationCustom06').value; // Code postal de la ville
const ville = document.querySelector('#validationCustom07').value; // Nom de la ville

// URL de l'API Adresse avec le code postal et le nom de la ville
const apiUrl = `https://api-adresse.data.gouv.fr/search/?q=${codePostal}+${ville}`;

// Fonction pour faire la requête HTTP
async function getCodeINSEE() {
    const codePostal = document.querySelector('#validationCustom06').value; // Code postal de la ville
    const ville = document.querySelector('#validationCustom07').value; // Nom de la ville
    const apiUrl = `https://api-adresse.data.gouv.fr/search/?q=${codePostal}+${ville}`;

    try {
        const response = await fetch(apiUrl);
        const data = await response.json();

        // Vérification si des résultats ont été retournés
        if(data.features.length > 0) {
            // Récupération du code INSEE du premier résultat
            const codeInsee = data.features[0].properties.citycode;
            console.log("Code INSEE:", codeInsee);
            document.querySelector('#validationCustom08').value = codeInsee;
        } else {
            console.log("Aucun résultat trouvé.");
        }
    } catch (error) {
        console.error("Erreur lors de la récupération des données:", error);
    }
}



function addCity(){
    // création de variable pour la valeur du champ code postal
    let zip = document.querySelector('#validationCustom06').value;
    // création de la constante apiUrl
    const apiUrl = `https://apicarto.ign.fr/api/codes-postaux/communes/${zip}`;
    // fonction fetch
    fetch(apiUrl)
        .then(reponse => reponse.json()
            .then(data =>{
                //affichage dans la console de la réponse du fetch
                console.log(data);
                for (let i = 0; i < data.length; i++) {
                    let City = document.querySelector('#validationCustom07');
                    let option = document.createElement('option');
                    option.textContent = data[i].nomCommune;
                    City.add(option);
                }    
            }))
}




function validate(){
    /*------------------ suppression des erreurs ----------------------*/
    const elements = document.getElementsByClassName("non_ok");
    while (elements.length > 0) elements[0].remove();
    /*------------------ controle d'erreur sur le prénom ----------------------*/

/*------------------ controle d'erreur sur le mot de pass ----------------------*/
    let password1 = document.querySelector('#inputPassword1').value;
    if (password1.length < 10){
        // on crée un element div8
        let div8 = document.createElement('div');
        // on lui donn une class non_ok
        div8.className = "non_ok";
        // on le rattache à son parent : password1
        document.querySelector('.password1').appendChild(div8);

        if (password1.length == 0){
        // on lui donne un contenu "champ pas être vide"
        div8.textContent = "champ doit pas être vide";
        }else{
        // on lui donne un contenu "champ trop court"
        div8.textContent = "champ trop court";
        }
    }
// /*           ---------controle si password1 contien une majuscule---------    */
    // let str = document.querySelector('.password1').value;
    // if ( str != (/[A-Z]/g)) {
    //     // on crée un element div8
    //     let div8 = document.createElement('div');
    //     // on lui donn une class non_ok
    //     div8.className = "non_ok";
    //     // on le rattache à son parent : password1
    //     document.querySelector('.password1').appendChild(div8);
    //     // on lui donne un contenu "champ pas être vide"
    //     div8.textContent = "champ non conforme";
    // }
/*           ---------controle si password1 contien une majuscule---------    */
    // if (str != (/[0-9]/g)) {
    //     // on crée un element div8
    //     let div8 = document.createElement('div');
    //     // on lui donn une class non_ok
    //     div8.className = "non_ok";
    //     // on le rattache à son parent : password1
    //     document.querySelector('.password1').appendChild(div8);
    //     // on lui donne un contenu "champ pas être vide"
    //     div8.textContent = "champ non conforme";
    // }

/*------------------ controle d'erreur sur la vérification de mot de passe ----------------------*/
    let password2 = document.querySelector('#inputPassword2').value;
    if (password2.length < 10){
        // on crée un element div9
        let div9 = document.createElement('div');
        // on lui donn une class non_ok
        div9.className = "non_ok";
        // on le rattache à son parent : password2
        document.querySelector('.password2').appendChild(div9);

        if (password2.length == 0){
        // on lui donne un contenu "champ doit pas être vide"
        div9.textContent = "champ doit pas être vide";
        }else{
        // on lui donne un contenu "champ trop court"
        div9.textContent = "champ trop court";
        }
    }
    //controle de mot de passe identique
    if (password2 != password1){
        // on crée un element div8
        let div8 = document.createElement('div');
        // on lui donn une class non_ok
        div8.className = "non_ok";
        // on le rattache à son parent : password1
        document.querySelector('.password1').appendChild(div8);
        // on lui donne un contenu "champs doivent être identiques"
        div8.textContent = "champs doivent être identiques";

        // on crée un element div9
        let div9 = document.createElement('div');
        // on lui donn une class non_ok
        div9.className = "non_ok";
        // on le rattache à son parent : password2
        document.querySelector('.password2').appendChild(div9);
        // on lui donne un contenu "champs doivent être identiques"
        div9.textContent = "champs doivent être identiques";
    }

}
