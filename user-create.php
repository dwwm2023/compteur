<?php

include "connect_bdd.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $error = '';
    // check username
    $login = $_POST['login'];
    $chk_login = $bdd->prepare('SELECT * FROM studeffiusers WHERE login = :login');
    $chk_login->execute([':login' => $login]);
    $count = $chk_login->fetchColumn();

    if ($count > 0) {
        $error = 'Nom d\'utilisateur déjà utilisé.';
    } else {
        // check login
        $login = htmlspecialchars($_POST['login']);
        $chk_login = $bdd->prepare('SELECT * FROM studeffiusers WHERE login = :login');
        $chk_login->execute([':login' => $login]);
        $count2 = $chk_login->fetchColumn();

        if ($count2 > 0) {
            $error = 'Nom d\'utilisateurdéjà utilisé.';
        } else {
            // check password
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirm_password'];
            if (strlen($password) < 8 || !preg_match('/[A-Z]/', $password) ||
		!preg_match('/[0-9]/', $password) || !preg_match('/[!@#$%^&*]/', $password)) {
                $error = "Le mot de passe ne respecte pas les critères.";
            } elseif ($password !== $confirmPassword) {
                $error = "Les mots de passe ne correspondent pas.";
            } else {
                // Create
                $hash = password_hash(htmlspecialchars($password), PASSWORD_DEFAULT);

                $query = $bdd->prepare('INSERT INTO studeffiusers (login, password) VALUES (:login, :password)');
                $query->execute([
                    ':login' => htmlspecialchars($_POST['login']),
                    ':password' => $hash
                ]);
                header('Location: login.php');
                exit;
            }
        }
    }
}

include "includes/header.php";
?>


<!-- form -->
<section>
    <div class="contentBox">
        <div class="formBox">
            <h2>Account creation</h2>
            <form action="#" method="POST">
                <div class="chkfrm">
                    <span><?php if (isset($error)) {echo $error;} ?></span>
                </div>
                <div class="inputBx user">
                    <span>Login</span>
                    <input type="login" name="login" id="login">
                </div>
                <div class="inputBx user">
                    <span>Password</span>
                    <span>Must contain a minimum of 8 characters, including at least 1 uppercase letter, 1 digit,
                         and 1 special character. Example: Example9&</span>
                    <input type="password" name="password" id="password">
                </div>
                <div class="inputBx user">
                    <span>Confirm password</span>
                    <input type="password" name="confirm_password" id="confirm_password">
                </div>
                <div class="inputBx">
                    <input type="submit" value="Register">
                </div>
            </form>
        </div>
    </div>
</section>

<?php
include "includes/footer.php";    
?>
