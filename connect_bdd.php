<?php
$host = '';
$db   = '';
$user = '';
$pass = '';
$port = '3306';

require('.config.php');

try
    {
        $bdd = new PDO("mysql:host=$host;port=$port;dbname=$db;charset=utf8", $user, $pass);
    }
catch (Exception $e)
    {
            die('Erreur de connexion à la base de données :' . $e->getMessage());
    }
