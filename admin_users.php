<?php
include "includes/header.php";

if (!$admin) {
  header('Location: login.php');
  exit;
}

require('./connect_bdd.php');
$query = $bdd->prepare('SELECT * FROM studeffiusers');
$query->execute();
$data = $query->fetchAll();
?>

<div class="admin_user">
    <h2>Liste des utilisateurs</h2>
    <table class="table table-user">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Login</th>
          <th scope="col">Is Admin ?</th>
          <th scope="col">Modifier</th>
          <th scope="col">Supprimer</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($data as $login) {
          ?>
          <tr>
            <td><?php echo ($login['Id']); ?></td>
            <td><?php echo ($login['login']); ?></td>
            <td>
              <?php if ($user) { ?>
                <?php if ($login['admin'] == 1) { ?>
                  <i class="fas fa-check-square"></i>
                <?php } else { ?>
                  <i class="far fa-square"></i>
                <?php } ?>
              <?php } ?>
            </td>
            <td>
              <?php if ($admin) { ?>
                <a href="user_update.php?id=<?php echo $login['Id'] ?>&token=<?php echo $_SESSION['user']['token']; ?>"><button class="btn"><i class="fas fa-edit"></i></button></a>
              <?php } ?>
            </td>
            <td>
              <?php if ($admin) { ?>
                <a href="user_delete.php?id=<?php echo $login['Id'] ?>&token=<?php echo $_SESSION['user']['token']; ?>"><button class="btn" onclick="return confirm('Etes vous certain de vouloir supprimer ' + '<?php echo $login['login']; ?>'+ ' ?')"><i class="fas fa-trash-alt"></i></button></a>
                <?php } ?>
            </td>
          </tr>
          <?php
        }
        ?>  
      </tbody>
    </table>
</div>


<?php

include "includes/footer.php";    
?>

