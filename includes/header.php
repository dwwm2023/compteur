<?php
$user = null;
$admin = null;
if (session_status() !== PHP_SESSION_ACTIVE) {
  session_start();
}
if (isset($_SESSION['user'])) {
  if ($_SESSION['user']['admin'] === true) {
      $admin = $_SESSION['user'];
  } else {
      $user = $_SESSION['user'];
  }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Compteurs</title>
</head>
<header>
<div id="root">
  <div id="topnav" class="topnav">
    <a id="home_link" class="topnav_link" href="index.php">Home</a>

    <!-- Classic Menu -->
    <nav id="topnav_menu">

      <?php if ($user or $admin){ ?>
        <a class="topnav_link" href="add_storyteller.php?token=<?php echo $_SESSION['user']['token']; ?>">AJOUT COMPTEUR</a>
      <?php } ?>
      <div>
        <?php
        if ($admin){ ?>
          <div class="dropdown">
            <button class="dropbtn">ADMINISTRATION</button>
            <div class="dropdown-content">
              <a class="topnav_link" href="admin_users.php?token=<?php echo $_SESSION['user']['token']; ?>">UTILISATEURS</a>
              <a class="topnav_link" href="admin_StoryTellers.php?token=<?php echo $_SESSION['user']['token']; ?>">COMPTEURS</a>
            </div>
          </div>
        <?php } ?>
      </div>
    </nav>
    <nav id="topnav_menu2">
      <?php if (!$user && !$admin){ ?>
        <a class="topnav_link" href="login.php">CONNEXION</a>
        <?php } elseif ($user || $admin) { ?>
          <a class="topnav_link" href="logout.php">DECONNEXION</a>
        <?php } ?>
        <?php
        if (!$admin && !$user) { ?>
          <a class="topnav_link" href="user-create.php">CREER COMPTE</a>
        <?php }
        if ($admin || $user) { ?>
<a class="topnav_link" href="user-account.php?id=<?php echo $_SESSION['user']['id']; ?>&token=<?php echo $_SESSION['user']['token']; ?>"><?php echo $_SESSION['user']['login']; ?></a>
        <?php } ?>
      </nav>

    <a id="topnav_hamburger_icon" href="javascript:void(0);" onclick="showResponsiveMenu()">
      <!-- Some spans to act as a hamburger -->
      <span></span>
      <span></span>
      <span></span>
    </a>

    <!-- Responsive Menu -->
    <nav id="topnav_responsive_menu">
      <ul>
        <li><a href="index.php">HOME</a></li>
        <?php
        if ($user || $admin) { ?>
        <li><a href="add_storyteller.php?token=<?php echo $_SESSION['user']['token']; ?>">AJOUT COMPTEUR</a></li>
        <?php }
        if ($admin){ ?>
            <li><a class="topnav_link" href="admin_users.php?token=<?php echo $_SESSION['user']['token'];
            ?>">GESTION UTILISATEURS</a></li>
            <li><a class="topnav_link separate" href="admin_StoryTellers.php?token=<?php echo $_SESSION['user']['token']; 
            ?>">GESTION COMPTEURS</a></li>
        <?php } 
        ?>
        <hr>
        <?php
        if (!$user && !$admin){ ?>
          <li><a class="topnav_link" href="login.php">CONNEXION</a></li>
        <?php } elseif ($user || $admin) { ?>
          <li><a class="topnav_link separate" href="logout.php">DECONNEXION</a></li>
        <?php } ?>
        <hr>
        <?php if (!$user && !$admin){ ?>
          <li><a class="topnav_link" href="user-create.php">CREER COMPTE</a></li>
        <?php
        } ?>
        <?php
        if ($admin || $user) { ?>
          <li><a class ="topnav_link" href="user-account.php?id=<?php echo $_SESSION['user']['id']; ?>&token=<?php echo $_SESSION['user']['token']; ?>"><?php echo $_SESSION['user']['login']; ?></a></li>
        <?php } ?>
      </ul>
    </nav>
  </div>
</div>