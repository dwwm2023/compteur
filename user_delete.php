<?php
include "connect_bdd.php";

// Delete
if(!empty($_GET['id'])) {
        $query = $bdd->prepare('DELETE FROM studeffiusers WHERE id=:id');
        $query->execute([
           'id' => $_GET['id']
        ]);
        header('Location: admin_users.php');
        exit;
    }
// Delete

include "includes/header.php";

if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token'])) {
    exit("token périmé");
}