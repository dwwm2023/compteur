<?php

include "connect_bdd.php";
include "includes/header.php";

if (!$admin) {
  header('Location: login.php');
  exit;
}

//récupération dans $data des données de la table "studeffist"
$query = $bdd->prepare('SELECT * FROM studeffist');
$query->execute();
$data = $query->fetchAll();

?>

<div class="admin_fonction">
  <h2>Liste des compteurs</h2>
  <table class="table table-bordered custom-table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Numéro de voie</th>
        <th scope="col">Nom de voie</th>
        <th scope="col">Code Postal</th>
        <th scope="col">Ville</th>
        <th scope="col">Code INSEE</th>
        <th scope="col">Modifier</th>
        <th scope="col">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <?php
        foreach ($data as $StoryTeller) {
            ?>
            <tr class="fonction-row">
                <td><?php echo($StoryTeller['Id']); ?></td>
                <td><?php echo($StoryTeller['name']); ?></td>
                <td><?php echo($StoryTeller['firstname']); ?></td>
                <td><?php echo($StoryTeller['number']); ?></td>
                <td><?php echo($StoryTeller['street']); ?></td>
                <td><?php echo($StoryTeller['zipcode']); ?></td>
                <td><?php echo($StoryTeller['city']); ?></td>
                <td><?php echo($StoryTeller['insee']); ?></td>
                </td>
                <td><?php if ($admin){ ?><a href="StoryTeller_update.php?id=<?php echo $StoryTeller['Id'] ?>&token=<?php echo $_SESSION['user']['token']; ?>"><i class="fas fa-edit"></i></a> <?php } ?></td>
                <td><?php if ($admin){ ?><a href="StoryTeller_delete.php?id=<?php echo $StoryTeller['Id'] ?>&token=<?php echo $_SESSION['user']['token']; ?>"><button class="btn" onclick="return confirm('Etes vous certain de vouloir supprimer ' + '<?php echo $StoryTeller['name']; ?>'+ ' ?')"><i class="fas fa-trash-alt"></i></button></a> <?php } ?></td>
            </tr>
            <?php
        }
      ?>  
    </tbody>
  </table>
</div>

<?php
include "includes/footer.php";    
?>
