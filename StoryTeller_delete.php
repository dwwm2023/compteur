<?php


include "connect_bdd.php";

// Delete
if (!empty($_GET['id'])) {
    $query = $bdd->prepare('DELETE FROM studeffist WHERE id=:id');
    $query->execute([
        'id' => $_GET['id']
    ]);
    // Rediret after delete
    header('Location: admin_StoryTellers.php');
    exit();
}
// Delete

include "includes/header.php";

if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token'])) {
    exit("token périmé");
}