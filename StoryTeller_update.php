<?php


include "connect_bdd.php";

    
if (!empty($_GET['id'])) {
    // Sélectionnez les données de l'utilisateur à mettre à jour
    $query = $bdd->prepare('SELECT * FROM studeffist WHERE id=:id');
    $query->execute([
        'id' => $_GET['id']
    ]);

    $data = $query->fetch();


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $error = '';
        $idClient = htmlspecialchars($_POST['idClient']);
        $name = htmlspecialchars($_POST['name']);
        $firstname = htmlspecialchars($_POST['firstname']);
        $number = htmlspecialchars($_POST['number']);
        $street = htmlspecialchars($_POST['street']);
        $zipcode = htmlspecialchars($_POST['zipcode']);
        $city = htmlspecialchars($_POST['city']);
        $insee = htmlspecialchars($_POST['insee']);


        //vérification si le nom n'existe pas déjà
        if ($name != $data['name']) {
            $chk_name = $bdd->prepare('SELECT * FROM studeffist WHERE name = :name');
            $chk_name ->execute([':name' =>$name]);
            $count = $chk_name->fetchColumn();
    
            if ($count > 0) {
                $error = 'Le nom existe déjà existe déjà';
            }
        }

        if ($name != $data['name'] || $firstname != $data['firstname'] || $number != $data['number'] || $city != $data['city'] || $zipcode != $data['zipcode'] || $city != $data['city']) {
            if (empty($name) or empty($firstname) or empty($number) or empty($street) or empty($zipcode) or empty($city)) {
                $error = 'les champs ne doivent pas être vide';
            }    
        }

        if (!$error) {
        // update fonction in database
        $query = $bdd->prepare('UPDATE studeffist SET name=:name, firstname=:firstname,
            number=:number, street=:street, zipcode=:zipcode, city=:city, insee=:insee WHERE id=:idClient');
        $query->execute([
            'name' => $_POST['name'],
            'firstname' => $_POST['firstname'],
            'number' => $_POST['number'],
            'street' => $_POST['street'],
            'zipcode' => $_POST['zipcode'],
            'city' => $_POST['city'],
            'insee' => $_POST['insee'],
            'idClient' => $_GET['id']
        ]);

        // Redirect user after update
        header('Location: admin_StoryTellers.php?id=' . $_GET['id'] . '&modify=ok');
        exit;
        }
    }
} else {
    header('Location: admin_StoryTellers.php');
    exit;
}

include "includes/header.php";

if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token']))
{
    exit("token périmé");
}


?>

<div class="container-fluid">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-6">
                <div class="form">
                    <div class="head_form">
                        <h1 class="text-center">Mise à jour Compteur</h1>
                    </div>

                    <form class="row g-3 needs-validation" method="POST" novalidate>
                    <div class="row g-3 needs-validation" novalidate>
                <div class="firstName col-md-4">
                    <label for="validationCustom01" class="form-label">numéro client :</label>
                    <input type="text" class="form-control" name="idClient" id="validationCustom01" value="<?php echo $data['Id'] ?>">
                </div>

                <div class="firstName col-md-7">
                </div>
                

                <div class="firstName col-md-5">
                    <label for="validationCustom02" class="form-label">Nom :</label>
                    <input type="text" class="form-control" name="name" id="validationCustom02" value="<?php echo $data['name'] ?>">
                </div>

                <div class="lastName col-md-6">
                    <label for="validationCustom03" class="form-label">Prénom :</label>
                    <input type="text" class="form-control" name="firstname" id="validationCustom03" value="<?php echo $data['firstname'] ?>">
                </div>

                <div class="number col-md-2">
                    <label for="validationCustom04" class="form-label">N° rue :</label>
                    <input type="number" class="form-control" name="number" id="validationCustom04" value="<?php echo $data['number'] ?>">
                </div>
    
                <div class="road col-md-6">
                    <label for="validationCustom05" class="form-label">Nom de rue :</label>
                    <input type="text" class="form-control" name="street" id="validationCustom05" value="<?php echo $data['street'] ?>">
                </div>
    
                <div class="mail col-md-3">
                </div>

                <div class="zip col-md-3">
                    <label for="validationCustom06" class="form-label">Code postal :</label>
                    <input type="text" class="form-control" name="zipcode" id="validationCustom06" value="<?php echo $data['zipcode'] ?>">
                </div>

                <div class="city col-md-4">
                    <label for="validationCustom07" class="form-label">Ville :</label>
                    <select class="form-select" name="city" id="validationCustom07"><?php echo $data['city']; ?>
                    <option value="<?php echo $data['city']; ?>" ><?php echo $data['city']; ?></option>
                    </select>
                </div>

                <div class="city col-md-3">
                    <label for="validationCustom08" class="form-label">INSEE :</label>
                    <input type="text" class="form-control" name="insee" id="validationCustom08" value="<?php echo $data['insee'] ?>">
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" type="submit" onclick="validate()">Valider</button>
                </div>
            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include "includes/footer.php";    
?>
