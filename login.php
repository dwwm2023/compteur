<?php
session_start();
require('connect_bdd.php');

// Initialisation des messages d'erreur
$Error = "";

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $escaped_login = htmlspecialchars($_POST['login']);
    $query = $bdd->prepare('SELECT * FROM studeffiusers WHERE login = :login');
    $query->execute([
        'login' => $escaped_login
    ]);
    $data = $query->fetch();

    if ($data) {
        if (password_verify(htmlspecialchars($_POST['password']), $data['password'])){
            if ($data['admin'] == 1){
                $_SESSION['user']['admin'] = true;
            }
            else{
                $_SESSION['user']['admin'] = false;
            }
            $_SESSION['user']['id'] = $data['Id'];
            $_SESSION['user']['login'] = $data['login'];
            $_SESSION['user']['token'] = bin2hex(openssl_random_pseudo_bytes(15));
            header('Location: index.php');
            exit;
        }
        else{
            $Error = "login ou mot de passe incorrect";
        }
    }
    else{
        $Error = "login ou mot de passe incorrect";
    }
}

include "includes/header.php";
?>

<section>
    <div class="contentBox">
        <div class="formBox">
            <h2>login</h2>
            <form action="#" method="POST">
                <div class="chkfrm">
                    <span><?php if (isset($Error)) {echo $Error;} ?></span>
                </div>
                <div class="inputBx user">
                    <span>login</span>
                    <input type="text" name="login" id="login">
                </div>
                <div class="inputBx user">
                    <span>Password</span>
                    <input type="password" name="password" id="password">
                </div>
                <div class="inputBx user">
                    <input type="submit" value="Log In">
                </div>
                <div class="inputBx">
                    <span>Don't have an account ? <a href="./user-create.php">Sign In</a></span>
                </div>
            </form>
        </div>
    </div>
</section>

<?php
include "includes/footer.php";
?>
