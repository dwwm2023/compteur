<?php
include "connect_bdd.php";
include "includes/header.php";

if (!empty($_GET['id'])) {
    // Vérifiez si le formulaire a été soumis
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $error = '';

        // check login
        $id_login = $_GET['id'];
        $chk_name = $bdd->prepare('SELECT * FROM studeffiusers WHERE id=:id');
        $chk_name->execute([
            'id' => $_GET['id']
        ]);
        $result = $chk_name->fetch();

        if ($_POST['login'] !== $result['login']) {
            $error = 'Vous n\'avez pas la permission de changer le nom d\'utilisateur';
        } 

        // password
        if ($_POST['password'] !== $result['password']) {
            // check password
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirm_password'];

            if (strlen($password) < 8 || !preg_match('/[A-Z]/', $password) ||
	        	!preg_match('/[0-9]/', $password) || !preg_match('/[!@#$%^&*]/', $password)) {
                $error = "Le mot de passe ne respecte pas les critères.";
            } elseif ($password !== $confirmPassword) {
                $error = "Les mots de passe ne correspondent pas.";

            }
        }else{
            $password = $result['password'];
        }

        if (!$error) {
            // UPDATE
            if ($password != $result['password']) {
                $new_password = password_hash(htmlspecialchars($password), PASSWORD_DEFAULT);
            } else {
                $new_password = $result['password'];
            }
            $query = $bdd->prepare('UPDATE studeffiusers SET password=:password WHERE id=:id');
            $query->execute([
                'password' => $new_password,
                'id' => htmlspecialchars($_GET['id'])
            ]);

            // Redirect after update
            header('Location: index.php?id=' . $_GET['id'] . '&modify=ok');
            exit;
        }
        
    }

    if ((!empty($_GET['token']) && $_GET['token'] != $_SESSION['user']['token']) || empty($_GET['token'])) {
        exit("token périmé");
    }


    // Sélectionnez les données de l'utilisateur à mettre à jour
    $query = $bdd->prepare('SELECT * FROM studeffiusers WHERE id=:id');
    $query->execute([
        'id' => $_GET['id']
    ]);

    $data = $query->fetch();
} else {
    header('Location: index.php');
    exit;
}

?>

<section>
    <div class="contentBox">
        <div class="formBox">
            <h2>Update user by user</h2>
            <form action="#" method="POST">
                <div class="chkfrm">
                        <span><?php if (isset($error)) {echo $error;} ?></span>
                    </div>
                <div class="inputBx">
                <span>Nickname</span>
                    <input type="text" name="login" id="login" value="<?php echo $data['login'] ?>"  readonly>
                </div>
                <div class="inputBx write">
                    <span>Password</span>
                    <span>Doit contenir  8 caractères minimum, 1 majuscule, 1 chiffre et 1 caractère spécial. exemple : Exemple9&</span>
                    <input type="password" name="password" id="password" value ="<?php echo $data['password'] ?>">
                </div>
                <div class="inputBx">
                    <span>Confirm password</span>
                    <input type="password" name="confirm_password" id="confirm_password" value ="<?php echo $data['password'] ?>">
                </div>
                <div class="inputBx">
                    <input type="submit" value="Register">
                </div>
            </form>
        </div>
    </div>
</section>
